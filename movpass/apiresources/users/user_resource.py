import logging
from datetime import datetime
from flask import abort
from flask_restx import Resource
from movpass.config import api
from movpass.services.address_service import AddressService
from movpass.services.models import User, UserGender, Address
from movpass.apiresources.doc import user
from movpass.apiresources.schemas import *

log = logging.getLogger(__name__)

user_schema = UserSchema(many=False)

addresses_schema = AddressSchema(many=True)
address_schema = AddressSchema(many=False)

class UserResource(Resource):
    @api.doc(responses={200: "Success"})
    def get(self, user_id):
        log.debug("Getting addresses from user %s", user_id)
        try:
            addresses = AddressService.get_addresses(user_id)
            return addresses_schema.dump(addresses)
        except:
            log.exception("Failed to get addresses for user %s", user_id)
        
