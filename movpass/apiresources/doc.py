from flask_restx import fields

from movpass.config import api

address = api.model(
    "user_adress",
    {
        "street": fields.String(description="Street Name", required=True),
        "number": fields.Integer(description="House Numer", required=True),
        "city": fields.String(description="City Name", required=True),
        "zipCode": fields.String(description="Zipcode", required=True),
        "state": fields.String(description="State Name", required=True),
        "active": fields.Boolean(description="Is main address?", required=True),
    }
)

user = api.model(
    "user_data",
    {
        "name": fields.String(description="User Name", required=True),
        "email": fields.String(description="User Email", required=True),
        "phone": fields.String(description="User Phone", required=True),
        "gender": fields.String(description="User Gender [Options: male, female, other]", required=False),
        "birthdate": fields.Date(description="User birthdate (YYYY-MM-DD)", required=False),
        "addresses": fields.Nested(address)
    },
)
