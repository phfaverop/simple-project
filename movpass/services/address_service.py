import logging
from movpass.config import db
from movpass.services.models import Address

log = logging.getLogger(__name__)


class AddressService:
    @staticmethod
    def get_addresses(user_id):
        log.debug("Getting user address")
        try:
            addresses = Address.query.filter_by(user_id=user_id)
            log.debug("Found %s", addresses)
            return addresses
        except IndexError:
            log.exception("Invalid User")
            print("Invalid User")
        
        