import enum
from movpass.config import db
from sqlalchemy.orm import relationship


class UserGender(enum.Enum):
    male = "male"
    female = "female"
    other = "other"


class User(db.Model):
    __tablename__ = "users"

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(256), nullable=False)
    email = db.Column(db.String(256), nullable=False)
    phone = db.Column(db.String(256), nullable=True)
    gender = db.Column(db.Enum(UserGender), nullable=True)
    birthdate = db.Column(db.Date, nullable=True)
    addresses = relationship("Address")

class Address(db.Model):
    __tablename__ = "address"

    id = db.Column(db.Integer, primary_key=True)
    street = db.Column(db.String(256), nullable=False)
    number = db.Column(db.Integer, nullable=False)
    city = db.Column(db.String(256), nullable=True)
    zipCode = db.Column(db.String(256), nullable=True)
    state = db.Column(db.String(256), nullable=True)
    active = db.Column(db.Boolean, nullable=True)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'))