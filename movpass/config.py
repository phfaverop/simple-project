import os

from flask import Flask, Blueprint
from flask_marshmallow import Marshmallow
from flask_restx import Api
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate


basedir = os.path.abspath(os.path.dirname(__file__))

app = Flask(__name__)

blueprint = Blueprint("api", __name__, url_prefix="/api")
api = Api(
    blueprint,
    version="1.0",
    title="Movpass - API",
    description="First version of Movpass API",
    doc="/swagger/",
)
app.register_blueprint(blueprint)

# Build the Sqlite ULR for SqlAlchemy
DB_URL = os.getenv("DB_URL")
if not DB_URL:
    DB_URL = "postgresql+psycopg2://movpass:movpass@127.0.0.1:5432/movpass"

# Configure the SqlAlchemy part of the app instance
app.config["SQLALCHEMY_ECHO"] = True
app.config["SQLALCHEMY_DATABASE_URI"] = DB_URL
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False

# Create the SqlAlchemy db instance
db = SQLAlchemy(app)
migrate = Migrate(app, db)

# Initialize Marshmallow
ma = Marshmallow(app)
