import logging
from movpass.config import app, api
from movpass.apiresources.users.users_resource import UsersResource
from movpass.apiresources.users.user_resource import UserResource
from werkzeug.middleware.proxy_fix import ProxyFix

logging.basicConfig(level=logging.ERROR)
log = logging.getLogger(__name__)


client_namespace = api.namespace("users", description="Users Functions")
client_namespace.add_resource(UsersResource, "/")
client_namespace.add_resource(UserResource, "/<string:user_id>/addresses")

def create_app():
    app.wsgi_app = ProxyFix(app.wsgi_app)
    return app.wsgi_app


if __name__ == "__main__":
    app.run()
