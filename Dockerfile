FROM ubuntu:20.04

RUN apt update

RUN apt install -y python3-pip libsqlite3-dev libmariadbclient-dev

ADD requirements.txt requirements.txt 
RUN pip3 install -r requirements.txt

ENV FLASK_APP=movpass.server.py

ADD movpass /app/movpass
ADD migrations /app/migrations

WORKDIR /app

ENTRYPOINT ["waitress-serve", "--port=5000", "--call", "movpass.server:create_app"]

