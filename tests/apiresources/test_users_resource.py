import json

api_base_url = "/api/users/"

mimetype = "application/json"
headers = {
    "Content-Type": mimetype,
    "Accept": mimetype,
}


def test_list_clients(app_client_test):
    result = app_client_test.get(api_base_url)
    assert result.status_code == 200
    assert len(result.json) == 0


def test_save_client(app_client_test):
    data = {
        "name": "Afonso",
        "email": "afonso@standup.com.br",
        "phone": "+55419999-9991",
        "gender": "male",
        "birthdate": "1980-06-12",
    }

    result = app_client_test.post(api_base_url, data=json.dumps(data), headers=headers)
    assert result.status_code == 201

    data["id"] = 1
    assert result.json == data
